
This was a very enjoyable project and a great learning opportunity.
This is the link to the deployed version <a href= 'https://faculty-takehome-christian.netlify.app/'>Deployed</a>
This is the link to the figma I created in the planning phase <a href='https://www.figma.com/file/Qjch5T1dE5HuaWOBSHhQke/Faculty-takehome?node-id=0%3A1'>Figma</a>

Thank you for the opportunity :)